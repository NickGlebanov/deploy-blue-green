sudo apt update
sudo apt install nginx

curl -fsSL https://get.docker.com -o get-docker.sh
sudo mkdir /etc/apt/sources.list.d
sudo sh get-docker.sh
sudo mkdir /var/www/test-app
docker run -d --name gitlab-runner --restart always \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest
docker run --rm -it -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register

# примонтировать папку /var/www/ к файловой системе контейнера, добавив в файл config.toml
# в volumes путь к папке с правами на чтение и запись